+++
title = "Identity proof formats"
aliases = ["/proof-formats"]
weight = 4
+++

## The different formats

Identity proofs, the things that verify identity claims, can come in different forms. When adding new identity claims, always consult the documentation on this site for that specific service provider as some of them may not support all the different proof formats.

### URI

The simplest format is the **URI**:

```
FINGERPRINT_URI
```

Just add your key's fingerprint in URI form directly where needed.

Here is how to [obtain the FINGERPRINT_URI](/fingerprint).

### Message

Use the **Message** format to add some context:

```
[Verifying my cryptographic key: FINGERPRINT_URI]
```

Here is how to [obtain the FINGERPRINT_URI](/fingerprint).

<p class="info">The message itself is not part of the verification, only the <code>FINGERPRINT_URI</code> matters. So feel free to change the message or translate it in a different language.</p>

### Profile URL

Use the **URL** to your Keyoxide profile page as the proof:

```
https://keyoxide.org/FINGERPRINT
or
https://keyoxide.org/EMAIL
```

Here is how to [obtain the FINGERPRINT](/fingerprint). The URL may be part of a
sentence.

<p class="info">You may also use an Alias URL, which is simply a URL that directly redirects to a Keyoxide profile page. Be sure to use a 301 HTTP redirect and add the <code>Access-Control-Allow-Origin: *
</code> header.</p>

<p class="info">If you use a different Keyoxide instance, replace <code>keyoxide.org</code> with the instance's domain.</p>