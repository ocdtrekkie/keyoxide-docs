+++
title = "Ariadne Identity"
aliases = ["/ariadne", "/ariadne-id", "/ariadne-identity"]

weight = 1
+++

## What is an Ariadne Identity?

You might have noticed the term _Ariadne Identity_ pop up in some places, such as the OpenPGP profile notation syntax:

```
proof@ariadne.id=https://someservice.tld/user
```

This means, according to the [RFC 4880](https://datatracker.ietf.org/doc/html/rfc4880) (paraphrasing):

> Treat **https://someservice.tld/user** as a **proof**, whatever a **proof** is according to **ariadne.id**.

So `ariadne.id` is what we call a namespace, but what does it refer to?

## The Ariadne Spec

The [Ariadne Spec](https://ariadne.id) is our attempt to:

- create a community-driven living document that explains how decentralized online identities should work, and
- create an ecosystem of code and apps that no single entity rules but everyone can benefit from.

The Ariadne Spec(ification) is the text document that contains all the knowledge one needs to work with these decentralized online identities and build websites and services like Keyoxide.

Since the Ariadne Spec's content is continuously subject to change, it is called a _living_ document.

The Ariadne Spec is also _community-driven_: anyone can contribute if they wish to, not just the people involved in the Keyoxide project.

This means that Keyoxide is really just an implementation of the Ariadne Spec: an actor in an ecosystem that could never be dominated by a single entity since the knowledge and its governance are open and accessible by all.

And that is just how we like our digital ecosystems.

## What about metacode.biz?

Keyoxide initially required all OpenPGP notations to use the `metacode.biz` namespace. This is because the concept that powers Keyoxide was previously developed [1] by the person who used their `metacode.biz` domain name as namespace.

If you are still using `metacode.biz` in your proofs, no need to worry! Keyoxide (and other projects that depend on the [doipjs](https://codeberg.org/keyoxide/doipjs) library) will always to continue to support `metacode.biz` claims for both backwards compatibility and sentimental reasons.

Due to the number of `metacode.biz` claims still out there, we recommend developers of new implementations do the same and support these as well.

[1] As it turns out, the concept of OpenPGP-based decentralized identities has been passed down by a small number of developers, each inspired by their predecessor's work and ideas. Keyoxide is simply the latest in that lineage. If you have an idea on how to improve the concept, share it with the world and potentially use your own namespace :)