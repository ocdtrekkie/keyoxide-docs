+++
title = "Communauté Keyoxide"
aliases = ["/fr/community"]

weight = 5
+++

La communauté Keyoxide est ouverte à tous et à toutes contributions.

Il y a le [forum](https://community.keyoxide.org/) pour les grandes discussions qui vont définir le futur du projet Keyoxide.

Il y a le lobby IRC **#keyoxide** sur [libera.chat](https://libera.chat/) et le groupe Matrix [#keyoxide:matrix.org](https://matrix.to/#/#keyoxide:matrix.org) pour les discussions rapides et quotidiennes. Les deux sont synchronisés entre eux, il suffit d'en rejoindre un pour suivre toute la conversation.

Il y a aussi le [blog](https://blog.keyoxide.org/) avec flux atom/RSS pour suivre le progrès du projet Keyoxide.

Le code source se trouve sur [codeberg.org/keyoxide](https://codeberg.org/keyoxide), c'est aussi l'endroit pour nous faire parvenir les bugs. Toute contribution de code est le bienvenu !