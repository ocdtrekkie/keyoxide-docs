+++
title = "Verifying Keyoxide profiles"

weight = 1
+++

## Obtaining a Keyoxide profile

In order to verify someone's Keyoxide profile, you must obtain their secure document that contains it.

Keyoxide profiles come in two forms: [signature profiles](/signature-profiles) and [OpenPGP profiles](/openpgp-profiles). **Signature profiles** are text documents signed by a cryptographic key that contain the identity claims of the Keyoxide profile. **OpenPGP profiles** are OpenPGP public keys that contain the identity claims as notations.

You obtain either someone's signature profile or their OpenPGP public key and run it through any of the [Keyoxide clients](/clients).

## Verifying a signature profile

Here's what a signature profile looks like:

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Hey there! Here's a signature profile with proofs related to the DOIP project (https://doip.rocks).

Verify this profile at https://keyoxide.org/sig

proof=dns:doip.rocks
proof=https://fosstodon.org/@keyoxide
-----BEGIN PGP SIGNATURE-----

iQHEBAEBCgAuFiEENjcgJSPnwTCat56Z7y3FgntEX0sFAl/7L0MQHHRlc3RAZG9p
cC5yb2NrcwAKCRDvLcWCe0RfS3iYC/0QQqz2lzSNrkApdIN9OJFfd/sP2qeGr/uH
98YHa+ucwBxer6yrAaTYYuBJg1uyzdxQhqF2jWno7FwN4crnj15AN5XGemjpmqat
py9wG6vCVjC81q/BWMIMZ7RJ/m8F8Kz556xHiU8KbqLNDqFVcT35/PhJsw71XVCI
N3HgrgD7CY/vIsZ3WIH7mne3q9O7X4TJQtFoZZ/l9lKj7qk3LrSFnL6q+JxUr2Im
xfYZKaSz6lmLf+vfPc59JuQtV1z0HSNDQkpKEjmLeIlc+ZNAdSQRjkfi+UDK7eKV
KGOlkcslroJO6rT3ruqx9L3hHtrM8dKQFgtRSaofB51HCyhNzmipbBHnLnKQrcf6
o8nn9OkP7F9NfbBE6xYIUCkgnv1lQbzeXsLLVuEKMW8bvZOmI7jTcthqnwzEIHj/
G4p+zPGgO+6Pzuhn47fxH+QZ0KPA8o2vx0DvOkZT6HEqG+EqpIoC/a7wD68n789c
K2NLCVb9oIGarPfhIdPV3QbrA5eXRRQ=
=QyNy
-----END PGP SIGNATURE-----
```

Go to the [keyoxide.org/sig](https://keyoxide.org/sig) page, copy and paste the signature in the field and press the button to view the Keyoxide profile and verify its associated identity claims.

## Verifying an OpenPGP profile

OpenPGP public keys are usually not distributed directly as documents, but rather through identifiers that can then be used to fetch the OpenPGP public key.

### Fingerprint identifier

One such identifier is the key's fingerprint. The fingerprint — as the name suggests — uniquely identifies the key. Here's an example of a fingerprint:

```
3637202523e7c1309ab79e99ef2dc5827b445f4b
```

This identifier can then be used to query keyservers such as [keys.openpgp.org](https://keys.openpgp.org), which is used by default by the Keyoxide clients.

Go to the [keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b](https://keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b) page. The website will automatically find the public key for you and start the identity verification process.

### Email address identifier

A different identifier is the email address associated with the OpenPGP public key. They too can be used to fetch keys stored on the [keys.openpgp.org](https://keys.openpgp.org) keyserver.

Go to the [keyoxide.org/hkp/test@doip.rocks](https://keyoxide.org/hkp/test@doip.rocks) page. The website will automatically find the public key for you and start the identity claims verification process.

An email address identifier can also be used to obtain a public key not via keyservers but the so-called [Web Key Directory](/web-key-directory) protocol, used to fetch public keys directly from private domains.

Go to the [keyoxide.org/test@doip.rocks](https://keyoxide.org/test@doip.rocks) page. The website will automatically find the public key for you and start the identity verification process.

<p class="info">Keyoxide URLs specify how the website should fetch the key: <code>keyoxide.org/hkp/…</code> will use keyservers, <code>keyoxide.org/wkd/…</code> will use the WKD protocol. If no protocol is specified, a keyserver will be used for fingerprints. For email addresses, it will first try the WKD protocol and fall back to the keyserver.</p>

## Verify proofs

Open the [keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b](https://keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b) page.

You now see a list of domains and/or accounts on platforms for which the owner of the public key claims to have an control over. If you see a green tick next to it, the identity claim was verified: it really is their account! If you see a red cross, whoops, either they made a mistake or trying to claim an account which isn't theirs.