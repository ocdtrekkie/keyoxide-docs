+++
title = "Twitter"

[extra]
claim_syntax = "https://twitter.com/USERNAME/status/TWEET_ID"
claim_variables = ["USERNAME", "TWEET_ID"]
+++

Log in to [twitter.com](https://twitter.com) and compose a new tweet containing the proof.

{{ proof_formats() }}

After posting, copy the link to the tweet.