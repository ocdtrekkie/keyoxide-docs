+++
title = "Hackernews"

[extra]
claim_syntax = "https://news.ycombinator.com/user?id=USERNAME"
claim_variables = ["USERNAME"]
+++

Log in to [Hackernews](https://news.ycombinator.com), click on your **username** and add the proof to your **about** section.

{{ proof_formats() }}