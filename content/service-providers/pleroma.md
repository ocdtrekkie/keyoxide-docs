+++
title = "Pleroma"

[extra]
claim_syntax = "https://DOMAIN/users/USERNAME"
claim_variables = ["DOMAIN", "USERNAME"]
+++

Log in to your Pleroma instance and add the proof to your **Bio**.

{{ proof_formats() }}