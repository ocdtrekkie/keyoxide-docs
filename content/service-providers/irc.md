+++
title = "IRC"

[extra]
claim_syntax = "irc://IRC_SERVER/NICK"
claim_variables = ["IRC_SERVER", "NICK"]
+++

Log in to the IRC server with your registered nickname and send the
following message:

```
/msg NickServ SET PROPERTY KEY FINGERPRINT_URI
```

Here is how to [obtain the FINGERPRINT_URI](/fingerprint).

To check whether successful, send (make sure to replace NICK):

```
/msg NickServ TAXONOMY NICK
```

To add more fingerprints, send:

```
/msg NickServ SET PROPERTY KEY2 FINGERPRINT_URI
```