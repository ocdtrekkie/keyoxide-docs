+++
title = "dev.to"

[extra]
claim_syntax = "https://dev.to/USERNAME/POST_TITLE"
claim_variables = ["USERNAME", "POST_TITLE"]
+++

Log in to [dev.to](https://dev.to) and create a new post containing the proof.

{{ proof_formats() }}

After posting, copy the link to the post. It should look like `https://dev.to/USERNAME/POST_TITLE`.