+++
title = "Gitea"

[extra]
claim_syntax = "https://DOMAIN/USERNAME/gitea_proof"
claim_variables = ["DOMAIN", "USERNAME"]
+++

Log in the Gitea instance and click on **Create new repository**.

Set the repository name to **gitea_proof**.

Set the project description to the proof.

{{ proof_formats() }}

After creating the project, copy the link to the project.