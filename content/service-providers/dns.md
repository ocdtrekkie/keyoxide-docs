+++
title = "DNS"

[extra]
claim_syntax = "dns:DOMAIN?type=TXT"
claim_variables = ["DOMAIN"]
+++

Add a TXT record to the DNS records of the (sub)domain containing the proof. No specific TTL value is required.

{{ proof_formats() }}