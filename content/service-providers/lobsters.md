+++
title = "Lobste.rs"

[extra]
claim_syntax = "https://lobste.rs/u/USERNAME"
claim_variables = ["USERNAME"]
+++

Log in to [Lobste.rs](https://lobste.rs) and append the proof to the **About** section.

{{ proof_formats() }}