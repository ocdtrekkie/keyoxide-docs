+++
title = "Mastodon"

[extra]
claim_syntax = "https://DOMAIN/@USERNAME"
claim_variables = ["DOMAIN", "USERNAME"]
+++

Log in to your Mastodon instance and click on **Edit profile**.

Add a new item under **Profile metadata** with a label of your choosing — **OpenPGP**, **Cryptography** or **Keyoxide** could be a meaningful label. The value should be the proof.

{{ proof_formats() }}