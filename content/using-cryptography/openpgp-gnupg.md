+++
title = "OpenPGP with GnuPG"

weight = 1

[extra]
toc = true
+++

## Installation

<p class="warning">Working with GnuPG requires you to work with the command line! Recommended only for people with advanced computer skills.</p>

Install the **gpg** command line tool. It comes pre-installed with most Linux distributions. Mac users can use [GPGTools](https://gpgtools.org/). Windows users will need [GPG4win](https://www.gpg4win.org/).

## Generate a keypair

Generate a new cryptographic key:

```bash
gpg --full-gen-key
```

Choose **RSA and RSA** by just pressing enter:

```
Please select what kind of key you want:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
  (14) Existing key from card
Your selection?
```

Choose **3072** by just pressing enter:

```
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072)
```

<p class="info">Feel free to choose 4096 bits for increased security if you know what you are doing. 3072 bits is the current default of GPG and should be secure enough for most use cases.</p>

Choose **2y** to make your key valid for two years:

```
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 2y
```

<p class="warning">Set yourself a reminder in a calendar to extend this expiry date after a year! You can still extend the expiry date of an already expired key. Use the steps described <a href="#Changing_the_key_expiry_date">below</a>.</p>

Confirm this:

```
Key expires at Sun 28 Aug 2022 01:23:45 AM CEST
Is this correct? (y/N) y
```

Now, enter your name and email address (you can leave the comment empty):

```
GnuPG needs to construct a user ID to identify your key.

Real name: Alice Bobdottir
Email address: alice@bobdottir.net
Comment:
```

This email needs to be valid! You will receive an email later in this process.

Confirm this:

```
You selected this USER-ID:
    "Alice Bobdottir <alice@bobdottir.net>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? o
```

The next screen asks you to input a password to secure the key. Please use a long but memorable password and store it using a password manager: if you lose this password, you won't be able to edit the claims in the future or decrypt messages.

Once you confirm your password, the following message appears:

```
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
```

Once this is done, you have generated your OpenPGP key! To find your so-called **fingerprint**, run:

```bash
gpg --fingerprint
```

One of the first lines consists of 10 blocks of 4 hexadecimal characters: this is your **fingerprint**, the identifier that is globally unique to your key.

### Distributing via keys.openpgp.org

Your new OpenPGP keypair is stored locally on your computer — both the private key and the public key. To make your public key available to others, you need to upload it to a so-called keyserver. Keyoxide recommends [keys.openpgp.org](https://keys.openpgp.org) for this purpose.

#### First time

Export the public key to a file named **pubkey.asc** (make sure to replace EMAIL):

```bash
gpg --armor --export EMAIL > pubkey.asc
```

Go to [keys.openpgp.org/upload](https://keys.openpgp.org/upload) and upload your key using the form.

To prove ownership of the email address you entered in the key, the server will send you an email asking you to confirm by clicking a link. This step only needs to be done once and when completed, uploading becomes a lot simpler by following the instructions below.

#### Subsequent times

Simply run (make sure to replace FINGERPRINT):

```bash
gpg --keyserver hkps://keys.openpgp.org --send-keys FINGERPRINT
```

### Distributing via a WKD server

Please refer to the [Web Key Directory documentation](/web-key-directory).

### Changing the key expiry date

Edit the key (make sure to replace FINGERPRINT):

```bash
gpg --edit-key FINGERPRINT
```

Optionally, select the subkey(s) for which you want to change the expiry date:

```
key 1
```

If you do not select a subkey, you will change the expiry date of the primary key.

```
expire
```

Extend the expiration time by 2 years (or however long suits you):

```
Changing expiration time for the primary key.
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 2y
```

Save the changes:

```
save
```

## Obtaining the fingerprint

Run:

```bash
gpg -k
```

Find the correct key. It should look something like:

```
pub   rsa3072 2022-01-01 [SC]
      FINGERPRINT
uid           [ultimate] My Name <my@email.address>
```

There is the fingerprint you are looking for.

## Signing a document

Assuming you have a plaintext file at the location FILENAME.

Sign the document (make sure to replace EMAIL_ADDRESS and FILENAME):

```bash
# Simple binary signature
gpg -u EMAIL_ADDRESS --sign FILENAME
# Simple armored signature
gpg -u EMAIL_ADDRESS --armor --sign FILENAME

# Clear signed (signature contains original message)
gpg -u EMAIL_ADDRESS --clear-sign FILENAME
```

## Verifying a signature

Assuming you have a signature file at the location FILENAME.

Verify the signature (make sure to replace FILENAME):

```bash
gpg --verify FILENAME
```

## Working with notations

Notations are small pieces of information (key-value pairs) one can add to an OpenPGP keypair.

Edit the key (make sure to replace FINGERPRINT):

```
gpg --edit-key FINGERPRINT
```

Get a list of user IDs and find the index of the one to add the identity claim to:

```
list
```

Select the desired user ID (make sure to replace N):

```
uid N
```

<p class="info">If you don't select a user ID and your key has multiple user IDs, the notation will be added to all of them.</p>

Add a new notation:

```
notation
```

Enter the notation and hit the **enter** key to confirm:

```
key=value
```

To delete an existing notation, "add" it again with a **-** (minus) symbol in front, as such:

```
-key=value
```

<p class="info">You can also add <code>none</code> as notation to delete all notations.</p>

List all the notations:

```
showpref
```

Save the key:

```
save
```