+++
title = "Overview"
aliases = ["/openpgp-profiles"]

weight = 0
+++

OpenPGP profiles are based on so-called notations, which are little pieces of data that you can store inside an [OpenPGP public key](/openpgp) — that is how the identity claims are stored and distributed for verification by others.

## Create and manage an OpenPGP profile

There is currently one supported way to create and manage such a profile:

- [using GnuPG](/openpgp-profiles/using-gnupg)