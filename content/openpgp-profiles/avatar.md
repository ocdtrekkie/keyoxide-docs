+++
title = "Adding an avatar"

weight = 2

[extra]
toc = true
+++

<p class="warning">Signature profiles are an experimental feature and may be subject to changes!</p>

## Adding an avatar

### Libravatar 

You can [create an account on Libravatar.org](https://www.libravatar.org/) or [run your own instance](https://wiki.libravatar.org/running_your_own/), both methods are supported by Keyoxide.

<p class="info">Make sure your Libravatar account's email address is the same as the one in your OpenPGP key.</p>

Log in to your Libravatar account and upload a profile image.

### Gravatar 

If you prefer to use Gravatar, you can create an account on [Gravatar.com](https://gravatar.com/).

<p class="info">Make sure your Gravatar account's email address is the same as the one in your OpenPGP key.</p>

Log in to your Gravatar account and upload a profile image.