+++
title = "Using GnuPG"

weight = 1

[extra]
toc = true
+++

<p class="warning">Working with GnuPG requires you to work with the command line! Recommended only for people with advanced computer skills.</p>

## Creating a profile

To create an OpenPGP profile, you need to generate your own [OpenPGP key pair](/openpgp). You only need to generate this key pair once. After this step, you will add and remove claims using always the same key pair.

To generate your own OpenPGP key pair, follow the instructions in the [OpenPGP with GnuPG guide](/using-cryptography/openpgp-gnupg) or the [OpenPGP with Kleopatra guide](/using-cryptography/openpgp-kleopatra).

## Adding an identity claim

<p class="warning">All identity claims stored inside keys start with <b>proof@ariadne.id=</b>. Never change this part! More information on the <a href="/ariadne-identity">Ariadne Identity</a> page.</p>

Edit the key (make sure to replace FINGERPRINT):

```bash
gpg --edit-key FINGERPRINT
```

Get a list of user IDs and find the index of the one to add the identity claim to:

```
list
```

Select the desired user ID (make sure to replace N):

```
uid N
```

<p class="warning">If you don't select a user ID and your key has multiple user IDs, the identity claim will be added to all of them. It's best to avoid this as it may cause redundant identity verifications.</p>

Add a new notation:

```
notation
```

On this website, from the menu on the left, choose a service provider for which to verify an account. Follow the instructions on that page.

Enter the notation (make sure to replace CLAIM with the information from the documentation page):

```
proof@ariadne.id=CLAIM

# Example: proof@ariadne.id=dns:yourdomain.org?type=TXT
```

Save the key:

```
save
```

Upload the updated OpenPGP key to [keys.openpgp.org](/using-cryptography/openpgp-gnupg#Distributing_via_keys.openpgp.org) or [Web Key Directory](/using-cryptography/openpgp-gnupg#Distributing_via_a_WKD_server).

## Listing identity claims

Edit the key (make sure to replace FINGERPRINT):

```bash
gpg --edit-key FINGERPRINT
```

List detailed preferences:

```
showpref
```

You should now see your key details, uid, and proofs assigned to your keys:

```
[ultimate] (1). Your Name <your@email>
  Cipher: AES256, AES192, AES, 3DES
  Digest: SHA512, SHA384, SHA256, SHA1
  Compression: ZLIB, BZIP2, ZIP, Uncompressed
  Features: MDC, Keyserver no-modify
  Notations: proof@ariadne.id=dns:yourdomain.org?type=TXT
```

Exit gpg:

```
quit
```

## Deleting an identity claim

Edit the key (make sure to replace FINGERPRINT):

```bash
gpg --edit-key FINGERPRINT
```

Launch the notation prompt:

```
notation
```

Enter the `-` (minus) symbol followed by the proof you want to delete:

```
-proof@ariadne.id=CLAIM

# Example: -proof@ariadne.id=dns:yourdomain.org?type=TXT
```

<p class="info">To make it easier to enter the right proof, you could first <a href="#Listing_claims">list all the claims</a> and simply copy the proof (including "proof@ariadne.id=") you want to delete.</p>

<p class="info">You can also enter <code>none</code> to delete all notations.</p>

Save the changes:

```
save
```

Upload the updated OpenPGP key to [keys.openpgp.org](/using-cryptography/openpgp-gnupg#Distributing_via_keys.openpgp.org) or [Web Key Directory](/using-cryptography/openpgp-gnupg#Distributing_via_a_WKD_server).

## Adding an avatar

### Libravatar 

You can [create an account on Libravatar.org](https://www.libravatar.org/) or [run your own instance](https://wiki.libravatar.org/running_your_own/), both methods are supported by Keyoxide.

<p class="info">Make sure your Libravatar account's email address is the same as the one in your OpenPGP key.</p>

Log in to your Libravatar account and upload a profile image.

### Gravatar 

If you prefer to use Gravatar, you can create an account on [Gravatar.com](https://gravatar.com/).

<p class="info">Make sure your Gravatar account's email address is the same as the one in your OpenPGP key.</p>

Log in to your Gravatar account and upload a profile image.